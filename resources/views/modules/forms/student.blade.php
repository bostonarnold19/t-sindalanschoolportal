@extends('layouts.dashboard')
@section('content')
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Student Request Form</h4>
    </div>
</div>
<div class="row">
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <strong>Success!</strong> {{ Session::get('flash_message') }}
    </div>
    @endif
</div>
<div class="row">
    @if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{$error}}
    </div>
    @endforeach
    @endif
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <form  onsubmit="return checkform(this);" id="xx" action="{{ route('post.student.fuckshit') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="name">Name</label>
                            <input required value="{{ old('name') }}" id="name" name="name" type="text" placeholder="Name" class="form-control input-md">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="request">Request Document</label>
                            <select required id="request" name="request" class="form-control">
                                <option {{ old('request') === null ? 'selected' : '' }} disabled>Select Request Document</option>
                                <option value="FORM 137" {{ old('request') === 'FORM 137' ? 'selected' : '' }}>FORM 137</option>
                                <option value="Good Moral Character" {{ old('request') === 'Good Moral Character' ? 'selected' : '' }}>Good Moral Character</option>
                                <option value="Certificate of Enrollment" {{ old('request') === 'Certificate of Enrollment' ? 'selected' : '' }}>Certificate of Enrollment</option>
                                <option value="gago">Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input style="display: none;" type="text" placeholder="OTHERS" class="form-control input-md" name="request_text" id="text-request">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="capbox">
                                <div id="CaptchaDiv"></div>
                                <div class="capbox-inner">
                                    Type the above number:<br>
                                    <input type="hidden" id="txtCaptcha">
                                    <input type="text" name="CaptchaInput" id="CaptchaInput" size="15"><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn form-control btn-default">Apply</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">

// Captcha Script

function checkform(theform){
var why = "";

if(theform.CaptchaInput.value == ""){
why += "- Please Enter CAPTCHA Code.\n";
}
if(theform.CaptchaInput.value != ""){
if(ValidCaptcha(theform.CaptchaInput.value) == false){
why += "- The CAPTCHA Code Does Not Match.\n";
}
}
if(why != ""){
alert(why);
return false;
}
}

var a = Math.ceil(Math.random() * 9)+ '';
var b = Math.ceil(Math.random() * 9)+ '';
var c = Math.ceil(Math.random() * 9)+ '';
var d = Math.ceil(Math.random() * 9)+ '';
var e = Math.ceil(Math.random() * 9)+ '';

var code = a + b + c + d + e;
document.getElementById("txtCaptcha").value = code;
document.getElementById("CaptchaDiv").innerHTML = code;

// Validate input against the generated number
function ValidCaptcha(){
var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
var str2 = removeSpaces(document.getElementById('CaptchaInput').value);
if (str1 == str2){
return true;
}else{
return false;
}
}

// Remove the spaces from the entered and generated code
function removeSpaces(string){
return string.split(' ').join('');
}
</script>


      <script>
            function recaptcha_callback(){
                    document.getElementById('xx').submit();
    }


                                $('#request').on('change', function() {
                                    if($(this).val() == 'gago') {
                                        $('#text-request').show();
                                    } else {
                                        $('#text-request').hide();
                                        $('#text-request').val('');


                                    }
                                });
                            </script>

@stop
